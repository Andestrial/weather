package com.sergey_zorych.tmdb.data.di

import com.sergey_zorych.tmdb.data.repositories.ApiRepositoryImp
import com.sergey_zorych.tmdb.domain.repositories.ApiRepository
import org.koin.dsl.module

val repositoriesModule = module {
    single<ApiRepository> { ApiRepositoryImp(get()) }
}